
//COUNTDOWN
$(document).ready(function() {
	var date = new Date("October 14, 2017 17:30:00");
    var now = new Date();
    var diff = (date.getTime()/1000) - (now.getTime()/1000);

    var clock = $('.clock').FlipClock(diff,{
        clockFace: 'DailyCounter',
        countdown: true
    });

    clock.setTime(diff);
    clock.start();
    });

